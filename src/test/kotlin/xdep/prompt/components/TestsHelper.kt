package xdep.prompt.components

import xdep.prompt.core.Component
import xdep.prompt.core.KInquirerEvent


internal fun <T> Component<T>.onEventSequence(func: MutableList<KInquirerEvent>.() -> Unit) {
    val events = mutableListOf<KInquirerEvent>()
    events.func()
    events.forEach { event -> onEvent(event) }
}
