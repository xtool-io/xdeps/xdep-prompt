package xdep.prompt.core


fun validate(exp: Boolean, failureMessage: String) = ValidationContext(exp, failureMessage)
data class ValidationContext(val exp: Boolean, val failureMessage: String)
