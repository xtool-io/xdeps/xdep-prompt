package xdep.prompt

import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Component

@Component
@ComponentScan
class XdepPrompt {
}
