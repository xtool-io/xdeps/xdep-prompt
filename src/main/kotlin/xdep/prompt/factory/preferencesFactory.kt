package xdep.prompt.factory

import org.apache.commons.configuration2.XMLConfiguration
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder
import org.apache.commons.configuration2.builder.fluent.Configurations
import java.io.File
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.name

fun preferencesFactory(path: Path): FileBasedConfigurationBuilder<XMLConfiguration>? {
    if(!path.exists()) {
        File(path.name).writeText("""
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <configuration>
            </configuration>
        """.trimIndent())
    }
    val configs = Configurations()
    return configs.xmlBuilder(path.toFile())
}
