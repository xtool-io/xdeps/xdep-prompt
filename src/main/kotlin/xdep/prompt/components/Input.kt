package xdep.prompt.components

import xdep.prompt.Prompt
import xdep.prompt.core.KInquirerEvent.*
import org.fusesource.jansi.Ansi.*
import xdep.prompt.core.*
import xdep.prompt.core.toAnsi
import java.math.BigDecimal

internal class InputComponent(
    val message: String,
    val default: String = "",
    val hint: String = "",
    val history: History? = null,
    val validation: (s: String) -> ValidationContext = { ValidationContext(true, "") },
    val filter: (s: String) -> Boolean = { true },
    val transform: (s: String) -> String = { s -> s },
) : Component<String> {
    private var value: String? = null
    private var interacting = true
    private var errorMessage = ""

    override fun value(): String = value ?: default

    override fun isInteracting(): Boolean = interacting

    override fun onEvent(event: KInquirerEvent) {
        errorMessage = ""
        when (event) {
            is KeyPressEnter -> {
                val validationContext = validation(value())
                if (validationContext.exp) {
                    interacting = false
                } else {
                    errorMessage = validationContext.failureMessage
                }
            }

            is KeyPressBackspace -> {
                value = value?.dropLast(1)
            }

            is KeyPressSpace -> {
                if (filter(" ")) {
                    value = value?.plus(" ") ?: " "
                }
            }

            is Character -> {
                val tempVal = value.orEmpty() + event.c
                if (filter(tempVal)) {
                    value = tempVal
                }
            }

            else -> {}
        }
    }

    override fun render(): String = buildString {
        // Question mark character
        append("?".toAnsi { fgGreen(); bold() })
        append(" ")

        // Message
        append(message.toAnsi { bold() })
        append(" ")

        when {
            interacting && value.isNullOrEmpty() && hint.isNotBlank() -> {
                // Hint
                append("  ")
                append(hint.toAnsi { fgBrightBlack() })
                append(ansi().cursorLeft(hint.length + 2))
            }

            interacting -> {
                // User Input
                append(transform(value()))
                // Error message
                if (errorMessage.isNotBlank()) {
                    append("  ")
                    append(errorMessage.toAnsi { bold(); fgRed() })
                    append(ansi().cursorLeft(errorMessage.length + 2))
                }
            }

            else -> {
                // User Input with new line
                appendLine(transform(value()).toAnsi { fgCyan(); bold(); })
            }
        }
    }
}


public fun Prompt.promptInput(
    message: String,
    default: String = "",
    hint: String = "",
    history: History? = null,
    validation: (s: String) -> ValidationContext = { ValidationContext(true, "") },
    filter: (s: String) -> Boolean = { true },
    transform: (s: String) -> String = { it }
): String {
    return prompt(InputComponent(message, default, hint, history, validation, filter, transform))
}

public fun Prompt.promptInputPassword(
    message: String,
    default: String = "",
    hint: String = "",
    mask: String = "*"
): String {
    val validation: (s: String) -> ValidationContext = { ValidationContext(true, "") }
    val filter: (s: String) -> Boolean = { true }
    val transform: (s: String) -> String = { it.map { mask }.joinToString("") }

    return prompt(InputComponent(message, default, hint, null, validation, filter, transform))
}

public fun Prompt.promptInputNumber(
    message: String,
    default: String = "",
    hint: String = "",
    history: History? = null,
    transform: (s: String) -> String = { it }
): BigDecimal {
    val validation: (s: String) -> ValidationContext = { ValidationContext(it.matches("\\d+.?\\d*".toRegex()), "") }
    val filter: (s: String) -> Boolean = { it.matches("\\d*\\.?\\d*".toRegex()) }

    return BigDecimal(prompt(InputComponent(message, default, hint, history, validation, filter, transform)))
}
