package xdep.prompt

import org.jline.terminal.TerminalBuilder
import xdep.prompt.core.AnsiOutput
import xdep.prompt.core.Component
import xdep.prompt.core.KInquirerReaderHandler
import java.io.Reader

public object Prompt {

    val terminal = TerminalBuilder.builder()
        .jna(true)
        .system(true)
        .build()

    public fun <T> prompt(component: Component<T>): T {
        runTerminal { reader ->
            val readerHandler = KInquirerReaderHandler.getInstance()
            AnsiOutput.display(component.render())
            while (component.isInteracting()) {
                val event = readerHandler.handleInteraction(reader)
                component.onEvent(event)
                AnsiOutput.display(component.render())
            }
        }
        return component.value()
    }

    private fun runTerminal(func: (reader: Reader) -> Unit) {
//        val terminal: Terminal = TerminalBuilder.builder()
//            .jna(true)
//            .system(true)
//            .build()
        terminal.enterRawMode()
        val reader = terminal.reader()

        func(reader)

        reader.close()
        terminal.close()
    }
}
